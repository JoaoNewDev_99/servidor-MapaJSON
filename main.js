const express = require('express');
const srv = express();

srv.get('/', getRootJSON); // Aqui estamos em home = '/'(raiz)

srv.get('/info', getInfoJSON); // Exercicio = Ao consultar '/info' em URL, retorna a function 'getInfoJSON'

srv.get('/teste', (req, res)=>{
    const testeInfo = {
        teste: 'Teste Ok'
    }
    res.json(testeInfo);
})

srv.listen(3030);

function getRootJSON(pedido, resposta) {
    const homePage = {
        titulo: 'Meu servidor',
        subtitulo: 'Home Page',
        texto: 'Lorem ipsum dolor sic amet...'
    }

    resposta.json(homePage); // Retorna em formato JSON o objeto 'homePage'
}

function getInfoJSON(pedido, resposta) {
    const info = {
        nome: 'servidor',
        versao: '1.0.0',
        licenca: 'MIT'
    }
    resposta.json(info);
}

function getRootHTML(pedido, resposta) {
    const homePage = `
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Meu servidor</title>
</head>
<body>
    <h1>
        Meu Servidor
    </h1>
    <h2>
        Home Page
    </h2>
    <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente blanditiis, 
        nisi officia amet voluptatibus vitae assumenda, repellat iste libero odio eligendi earum, 
        similique sint tempore eveniet quas nemo. Aperiam, iusto?</p>
</body>
</html>
    
    
    
    `
    resposta.send(homePage);
    
}