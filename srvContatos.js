// DEFININDO UMA INTERFACE COM LISTA DE CONTATOS
// srvContatos = Servidor de contatos

const express = require('express'); // pacote Express
const contatos = require('./contatos.json'); /* require('exige') o caminho de './contatos.json'
                                                onde possui o Objeto Array com nomes e emails.
                                                Ou seja, a variável 'contatos' recebe esse .json */
const mapaContatos = require('./mapaContatos.json');

const srv = express(); // essencial


srv.get('/email/:email', (req, res)=>{
    const abc = true; 
    if (abc) {
        res.json(mapaContatos[req.params.email]);
    } else {
        res.json({
            erro: 'Erro',
            msg: 'Contato não encontrado'
        })
    }
});


srv.get('/contatos/json', (req, res)=>{
    res.json(contatos);
}); // Lista dos contatos em JSON


srv.get('/contatos/html', (req, res)=>{ 
    listaHTML = `<ul>`;
    for (let index = 0; index < contatos.length; index++) {
        const contato = contatos[index];
        listaHTML = listaHTML + `<li>${contato.nome} - ${contato.email}`
    }
    /*listaHTML = listaHTML + `<ul>`;*/
    res.send(listaHTML);
}); // Lista dos contatos em formato HTML, consultando o Objeto Array (for Loop)


srv.get('/contatos/csv', (req, res)=>{ 
    listaHTML = `Nomes - Emails\n`; //
    for (let index = 0; index < contatos.length; index++) {
        const ctt = contatos[index]; // Consultando o array
        listaHTML = listaHTML + `<li>${ctt.nome}, ${ctt.email}`
    }
    res.send(listaHTML);
}); // Lista dos contatos separados por vírgula (csv)


srv.listen(8080, ()=>{ // <- função anônima aqui
    console.log('Pronto para rodar.');
});